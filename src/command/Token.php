<?php

declare (strict_types=1);

namespace think\token\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Token extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('token')
            ->setDescription('the stoken command');
    }

    protected function execute(Input $input, Output $output)
    {
        $key  = md5(uniqid().time().rand(0, 60));
        $leeway = 3600 * 24;

        $path = root_path() . DIRECTORY_SEPARATOR.'.env';
        if (file_exists($path)
            && strpos(file_get_contents($path), '[JWT]')
        ) {
            $output->writeln('JWT_SECRET is exists');
        } else {
            file_put_contents(
                $path,
                PHP_EOL . "[JWT]" . PHP_EOL . "KEY=$key" . PHP_EOL . "LEEWAY=$leeway" . PHP_EOL,
                FILE_APPEND
            );
            $output->writeln('JWT_SECRET has created');
        }
    }
}
