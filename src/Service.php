<?php
declare (strict_types = 1);

namespace think\token;

class Service extends \think\Service
{
    public function register()
    {
        $this->commands(\think\token\command\Token::class);
    }

    public function boot()
    {
        // 服务启动
    }
}